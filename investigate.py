#!/usr/bin/python3
# Copyright 2018 Sumana Harihareswara, Licensed under the GPL

# This is an investigatory piece of data science to figure out
# when the IRS last updated its public File 990 dataset
# and to help me find the 990s for particular charities based on the EIN.
# Badly needs refactoring to deal with different years better.

# First, download the indices locally:
# wget https://s3.amazonaws.com/irs-form-990/index_2015.json # 76.60M
# wget https://s3.amazonaws.com/irs-form-990/index_2016.json # 111.44M

import arrow
import json


with open("index_2015.json") as f:
    irs_2015 = json.load(f)

with open("index_2016.json") as f:
    irs_2016 = json.load(f)

with open("index_2017.json") as f:
    irs_2017 = json.load(f)

with open("index_2018.json") as f:
    irs_2018 = json.load(f)

filings_length_2015 = len(irs_2015['Filings2015']) # 261034

filings_length_2016 = len(irs_2016['Filings2016']) # 378420

filings_length_2017 = len(irs_2017['Filings2017']) #

filings_length_2018 = len(irs_2018['Filings2018']) #

def find_2016_filing_by_ein(EIN): # EIN must be a *string*
    for i in range(1,filings_length_2016):
        if irs_2016['Filings2016'][i]['EIN'] == EIN:
            return(irs_2016['Filings2016'][i])

def find_2015_filing_by_ein(EIN):
    for i in range(1,filings_length_2015):
        if irs_2015['Filings2015'][i]['EIN'] == EIN:
            return(irs_2015['Filings2015'][i])

def find_2017_filing_by_ein(EIN):
    for i in range(1,filings_length_2017):
        if irs_2017['Filings2017'][i]['EIN'] == EIN:
            return(irs_2017['Filings2017'][i])

def find_2018_filing_by_ein(EIN):
    for i in range(1,filings_length_2018):
        if irs_2018['Filings2018'][i]['EIN'] == EIN:
            return(irs_2018['Filings2018'][i])

# >>> irs_2015['Filings2015'][90467]
# {'LastUpdated': '2016-03-21T17:23:53', 'ObjectId': '201511349349204916',
#    'DLN': '93492134049165', 'SubmittedOn': '2015-07-27',
#    'URL': 'https://s3.amazonaws.com/irs-form-990/201511349349204916_public.xml',
#    'EIN': '274103153', 'FormType': '990EZ', 'TaxPeriod': '201412',
#    'OrganizationName': 'STUMPTOWN SYNDICATE'}

# The ObjectId and the URL are pretty useful!

first_item_2016_updated = arrow.get(irs_2016['Filings2016'][0]['LastUpdated'])

first_item_2015_updated = arrow.get(irs_2015['Filings2015'][0]['LastUpdated'])

def get_last_submission_2016():
    for i in range(1, filings_length_2016):
        cur_best = first_item_2016_updated
        last_index = 0
        if arrow.get(irs_2016['Filings2016'][i]['LastUpdated']) > cur_best:
            cur_best = arrow.get(irs_2016['Filings2016'][i]['LastUpdated'])
            last_index = i
    print(cur_best.format())
    print(irs_2016['Filings2016'][last_index])

def get_last_submission_2015():
    for i in range(1, filings_length_2015):
        cur_best = first_item_2015_updated
        last_index = 0
        if arrow.get(irs_2015['Filings2015'][i]['LastUpdated']) > cur_best:
            cur_best_2015 = arrow.get(irs_2015['Filings2015'][i]['LastUpdated'])
            last_index = i
    print(cur_best.format())
    print(irs_2015['Filings2015'][last_index])

if __name__ == "__main__":
    get_last_submission_2016()
