Form 990 investigation
======================

Disclaimer
----------

This is basically a toy project that I use to `test Python packaging
on PyPI <https://test.pypi.org/project/Forms990-analysis/>`__. You
probably don't need to use it, since `ProPublica's nonprofit explorer
<https://projects.propublica.org/nonprofits/organizations/274103153>`__
does most of what you need.

About
-----

`A bunch of recent tax filings by US charitable organizations are
available on AWS <https://aws.amazon.com/public-datasets/irs-990/>`__
and I'm messily learning to navigate them.

This is an investigatory piece of data science to figure out when the
IRS last updated its public File 990 dataset, which it `initially
uploaded in mid-2016
<http://grantspace.org/blog/irs-releases-electronically-filed-form-990-data>`__,
and to help me find the 990s for particular charities based on the
EIN.  (`Here's a sample File 990, for the Stumptown Syndicate, in
2015. <https://s3.amazonaws.com/irs-form-990/201511349349204916_public.xml>`__)

To run this, you'll need to first download the indices locally:

``$ wget https://s3.amazonaws.com/irs-form-990/index_2015.json # 76.60M``

``$ wget https://s3.amazonaws.com/irs-form-990/index_2016.json # 111.44M``

and so on for 2017 and 2018.

Thanks to `Arrow <http://arrow.readthedocs.io/en/latest/index.html>`__,
`bpython <https://www.bpython-interpreter.org/>`__, and the Internal
Revenue Service for making this project possible or easier.

TODO
----

Badly-needed refactoring to deal with different years better.

Provide a proper CLI for ingesting an EIN and providing JSON.

Add ``tox`` configuration and tests.

|No Maintenance Intended|

.. |No Maintenance Intended| image:: http://unmaintained.tech/badge.svg
   :target: http://unmaintained.tech/
